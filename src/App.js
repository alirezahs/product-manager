import React, { Component } from 'react';
import { Header, Footer, Auth } from './inc/base';
import {User, Loading} from './inc/functions';
import Profile from './views/profile/profile';
import './static/css/bootstrap.min.css';
import './static/css/uikit.min.css';
import './static/css/style.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import {Menu} from './inc/menu';
import {Login} from './views/auth/login';
import { EmployeeEdit } from './views/employee/edit';
import { EmployeesList } from './views/employee/list';
import { PartNew } from './views/part/new';
import { PartsList } from './views/part/list';
// import { ProductNew } from './views/product/new';
import { ProductsList } from './views/product/list';
// import { ProductSearch } from './views/product/search';
import { Product } from './views/product/view';

import { PartPropertiesList } from './views/part_property/list';
import { PartInstructionsList } from './views/part_instruction/list';
import { PartPropertyNew } from './views/part_property/new';
import { PartInstructionNew } from './views/part_instruction/new';
import Logo from './static/img/logo.svg';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {}
    console.log(window.sessionStorage);
    if(window.location.pathname !== '/login/' && !User.is_user())
      window.location = '/login/';
  }
  render() {
    return (
      <Router>
        <div id="dashboard">
          {/* <Header/> */}
          <div id="loading">
            <div uk-spinner="ratio: 2"></div>
          </div>
          <div className="container-fluid p-0">
            <Auth access="not-user" redirect={false}>
                <Route path="/login/" exact component={Login} />
            </Auth>
            <Auth access="user">
              <div className="row m-0">
                <div className="col-12 col-md-3 col-lg-2 sidebar">
                    <div className="sidebar_header mt-5">
                        <img className="logo" src={Logo} />
                        <h1 className="sidebar__title">Product Manager</h1>
                    </div>
                    <Menu />
                </div>
                <div className="col-12 col-md-9 col-lg-10 main">
                  <Route path="/" exact component={Profile} />
                  
                  <Route path="/employees/" component={EmployeesList} />
                  <Route path="/employee/:username/" component={EmployeeEdit}/>
                  
                  <Route path="/parts/" exact component={PartsList} />
                  <Route path="/part/new/" exact component={PartNew} />
                  <Route path="/part/:id/edit/" exact component={(props) => <PartNew action="edit" {...props}/>} />
                  <Route path="/parts/instructions/" exact component={PartInstructionsList} />
                  <Route path="/parts/instruction/new/" exact component={PartInstructionNew} />
                  <Route path="/parts/instruction/:id/edit/" exact component={(props) => <PartInstructionNew action="edit" {...props}/>} />
                  <Route path="/parts/properties/" exact component={PartPropertiesList} />
                  <Route path="/parts/property/new/" exact component={PartPropertyNew} />
                  <Route path="/parts/property/:id/edit/" exact component={(props) => <PartPropertyNew action="edit" {...props}/>} />
                  
                  <Route path="/products/" component={ProductsList} />
                  <Route path="/product/:id/" component={Product} />
                </div>
              </div>
            </Auth>
          </div>
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
