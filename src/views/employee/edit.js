import React, {Component} from 'react';
import axios from 'axios';
import { User,Loading, BASE_URL } from '../../inc/functions';
import {Link} from "react-router-dom";

export class EmployeeEdit extends Component{
    constructor(props){
        super(props);
        this.state = {
            action : "edit",
            username : this.props.match.params.username,
            token: User.getToken(),
            instructions : [],
            allowed_instructions : [],
        }
        this.handleFormSubmit = this.handleFormSubmit.bind(this)
        this.isInstructionSelected = this.isInstructionSelected.bind(this)
        this.fillFormData = this.fillFormData.bind(this);
        Loading.start();
    }

    componentDidMount(){
        this.fillFormData();
        this.loadInstructions();
    }

    handleFormSubmit(e){
        e.preventDefault();
        Loading.start();
        var allowed = [];
        for (var i = 0; i < this.inputInstructions.length; i++) {
            if (this.inputInstructions.options[i].selected) allowed.push({ id: this.inputInstructions.options[i].value});
        }
        let data = {
            "user":{
                first_name: this.inputFirstName.value,
                last_name: this.inputLastName.value,
                allowed_instructions : allowed,
            }
        }
        axios
        .put(`${BASE_URL}employees/${this.state.username}/${this.state.token}/`,  data)
        .then(res => {
            console.log(res.data);
            Loading.stop();
            window.location = `/employees/`;
        });
        
    }
    // EDITING MODE
    fillFormData(){
        Loading.start();
        axios
            .get(`${BASE_URL}employees/${this.state.username}/${this.state.token}/`)
            .then(
                res => {
                    console.log(res.data);
                    this.inputFirstName.value = res.data.user.first_name;
                    this.inputLastName.value = res.data.user.last_name;
                    this.setState({allowed_instructions : res.data.user.allowed_instructions});
                    Loading.stop();
                }
            )
    }

    loadInstructions(){
        Loading.start();
        axios
            .get(`${BASE_URL}instructions/${this.state.token}/`)
            .then(
                res => {
                    console.log(res.data);
                    this.setState({instructions : res.data.instructions});
                    Loading.stop();
                }
            )
    }

    isInstructionSelected(id){
        for(var i in this.state.allowed_instructions)
            if(this.state.allowed_instructions[i].id === id) return true;
        return false;
    }

    render() {
        return (
            <div>
                <div className="uk-card uk-card-small">
                    <div className="uk-card-header">
                        <div className="row">
                            <div className="col-6">
                                <h3 className="uk-card-title uk-margin-remove-bottom">Edit Employee</h3>
                            </div>
                            <div className="col-6 uk-text-right">
                                <Link to="/employees/" className="uk-button uk-button-small uk-button-secondary">Back</Link>
                            </div>
                        </div>
                    </div>
                    <div className="uk-card-body">
                        <form action="#" method="POST" onSubmit={this.handleFormSubmit}>
                            <fieldset className="uk-fieldset">
                                <div className="uk-margin">
                                    <input className="uk-input" name="name" type="text" placeholder="First Name" ref={el => this.inputFirstName = el}/>
                                </div>
                                <div className="uk-margin">
                                    <input className="uk-input" name="name" type="text" placeholder="Last Name" ref={el => this.inputLastName = el}/>
                                </div>
                                <div className="uk-margin">
                                    <select className="uk-select" ref={el => this.inputInstructions = el} multiple={true}>
                                        {this.state.instructions.map((item, i) => {
                                            return(
                                                <option key={item.id} value={item.id} selected={this.isInstructionSelected(item.id) ? true : false}>{item.name}</option>
                                            )
                                        })}
                                    </select>
                                </div>
                                <button type="submit" name="general-submit" className="uk-button uk-button-primary uk-width-1-1">submit</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default EmployeeEdit;