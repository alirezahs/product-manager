import React, {Component} from 'react';
import axios from 'axios';
import { User,Loading, BASE_URL } from '../../inc/functions';
import {Link} from "react-router-dom";

export class EmployeesList extends Component{
    constructor(porps){
        super(porps);
        this.state = {
            list: [],
            token: User.getToken(),
            pendings: [],
        }
        this.acceptPending = this.acceptPending.bind(this);
        this.declinePending = this.declinePending.bind(this);
        this.promoteToAdmin = this.promoteToAdmin.bind(this);
        Loading.start();
    }


    componentDidMount() {
        this.load()
        this.loadPendings();
    }

    load(){
        Loading.start();
        axios
            .get(`${BASE_URL}employees/` + this.state.token + `/`)
            .then(
                res => {
                    console.log(res.data);
                    User.setToken(res.data.token);
                    this.setState({
                        list : res.data.users
                    });
                    Loading.stop();
                }
            )
    }

    loadPendings(){
        Loading.start();
        axios
            .get(`${BASE_URL}pending_email/`)
            .then(
                res => {
                    console.log(res.data);
                    this.setState({
                        pendings : res.data.pending_emails
                    });
                    Loading.stop();
                }
            )
    }

    acceptPending(e, id){
        Loading.start();
        let data = {
            id: id,
            factory_id : User.getUser().factory.id
        }
        axios
            .put(`${BASE_URL}pending_email/`, data)
            .then(
                res => {
                    console.log(res.data);
                    this.load();
                    this.loadPendings();
                    Loading.stop();
                }
            )
    }
    declinePending(e, id){
        Loading.start();
        axios
            .delete(`${BASE_URL}pending_email/${id}/`)
            .then(
                res => {
                    console.log(res.data);
                    this.load();
                    this.loadPendings();
                    Loading.stop();
                }
            )

    }

    promoteToAdmin(e, username){
        Loading.start();
        axios
        .get(`${BASE_URL}employees/${username}/promote/${this.state.token}/`)
        .then(
            res => {
                console.log(res.data);
                this.load();
                Loading.stop();
            }
        )
    }

    delete(id){
        if (!window.confirm("Are you sure to delete this item?")) return;
        Loading.start();
        axios
            .get(`${BASE_URL}employees/`+id+`/delete/` + this.state.token + `/`)
            .then(
                res => {
                    User.setToken(res.data.token);
                    this.load();
                    Loading.stop();
                }
            );
    }

    render() {
        return (
            <div>
                <div className="uk-card uk-card-small  uk-margin-bottom">
                    <div className="uk-card-header">
                        <h3 className="uk-card-title uk-margin-remove-bottom">Pending employees</h3>
                    </div>
                    <div className="uk-card-body">
                        <table className="uk-table uk-table-striped ">
                            <thead>
                                <tr>
                                    <th className="uk-table-expand">Email</th>
                                    <th>Name</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.pendings.map(
                                    (item, i) => {
                                        return(
                                            <tr key={i}>
                                                <td>{item.email}</td>
                                                <td>{item.first_name + " " + item.last_name}</td>
                                                <td>
                                                    <button onClick={(e) => {return this.acceptPending(e, item.id)}} className="uk-button uk-button uk-button-primary uk-button-small">Accept</button>
                                                    <button onClick={(e) => {return this.declinePending(e, item.id)}} className="uk-button uk-button uk-button-danger uk-button-small uk-margin-small-left">Decline</button>
                                                </td>
                                            </tr>
                                        )
                                    }
                                )}
                            </tbody>
                        </table>
                    </div>
                
                    <div className="uk-card-header">
                        <h3 className="uk-card-title uk-margin-remove-bottom">List of employees</h3>
                    </div>
                    <div className="uk-card-body">
                        <table className="uk-table uk-table-striped ">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Allowed Instructions</th>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.list.map(
                                    (item, i) => {
                                        return(
                                            <tr key={i}>
                                                <td>{i+1}</td>
                                                <td>{item.first_name + " " + item.last_name}</td>
                                                <td>
                                                    {item.allowed_instructions.map((i_item, i_i) => {
                                                        return(
                                                            i_item.name + ", "
                                                        )
                                                    })}
                                                </td>
                                                <td>
                                                    <button className="uk-button uk-button-danger uk-button-small " onClick={() => this.delete(item.username)}>Delete</button>
                                                    <Link to={'/employee/' + item.username + '/edit/'} className="uk-button uk-button-small uk-button-default uk-margin-small-left">Edit</Link>
                                                    {item.is_admin ? '' : <button onClick={(e) => {return this.promoteToAdmin(e, item.username)}} className="uk-button uk-button uk-button-primary uk-button-small uk-margin-small-left">Admin</button>} 
                                                </td>
                                            </tr>
                                        )
                                    }
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default EmployeesList;