import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { User, Loading } from '../../inc/functions';
import Logo from '../../static/img/logo.svg';

export class Login extends Component{
    constructor(props){
        super(props);
        this.handleLogin = this.handleLogin.bind(this);
        Loading.start();
    }

    handleLogin(e){
        e.preventDefault();
        Loading.start();
        var username = this.inputUserName.value;
        var password = this.inputPassword.value;
        User.login({username:username, password: password}, true);
    }

    render() {
        return (
            <div className="login">
                <div className="mx-auto uk-width-2-5">
                    <div className="logo_wrapper uk-text-center uk-margin-bottom">
                        <img src={Logo} className="logo" width="60"/>
                    </div>
                    <div className="uk-card uk-card-default uk-card-small">
                        <div className="uk-card-header">
                            <h3 className="uk-card-title uk-margin-remove-bottom">Login</h3>
                        </div>
                        <div className="uk-card-body">
                        <form onSubmit={this.handleLogin}>
                            <fieldset className="uk-fieldset">
                                <div className="uk-margin">
                                    <input className="uk-input" type="text" placeholder="Username" required={true} ref={el => this.inputUserName = el}/>
                                </div>
                                <div className="uk-margin">
                                    <input className="uk-input" type="password" placeholder="Password" required={true} ref={el => this.inputPassword = el}/>
                                </div>
                                <div className="uk-margin">
                                    <input className="uk-button uk-button-small uk-button-primary uk-width-1-1" type="submit" value="Sign In" />
                                </div>
                            </fieldset>
                        </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        );
    }
}

export default Login;