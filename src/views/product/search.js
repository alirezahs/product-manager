import React, {Component} from 'react';
import axios from 'axios';
import { User, BASE_URL } from '../../inc/functions';

export class ProductSearch extends Component{
    constructor(porps){
        super(porps);
        this.state = {
            list: [],
            token: User.getToken(),
            errors : [],
            messages : [],
        }
        // this.delete = this.delete.bind(this);
    }


    componentDidMount() {
        this.load()
    }

    load(){
        axios
            .get(`${BASE_URL}products/` + this.state.token + `/`)
            .then(
                res => {
                    console.log(res.data);
                    User.setToken(res.data.token);
                    this.setState({
                        list : res.data.products
                    })
                }
            )
    }

    searchProduct(e){
        e.preventDefault();
        let id = this.inputSearch.value;
        axios
            .get(`${BASE_URL}products/${id}/${this.state.token}`)
            .then(
                res => {

                }
            )
            .catch(
                err => {
                    this.setState({errors:['Product ID not found.']});
                }
            )
    }

    render() {
        return (
            <div>
                <div className="uk-card uk-card-small">
                    <div className="uk-card-header">
                        <h3 className="uk-card-title uk-margin-remove-bottom">Search product</h3>
                    </div>
                    <div className="uk-card-body">
                        <form onSubmit={this.searchProduct}>
                            {/* <legend class="uk-legend">Legend</legend> */}
                            <div className="uk-margin">
                                <div className="row justify-content-center">
                                    <div className="col-4 p-1">
                                        <input className="uk-input" type="text" placeholder="Enter product ID here" ref={el => this.inputSearch = el}/>
                                    </div>
                                    <div className="col-2 p-1">
                                        <button type="submit" className="uk-button uk-button-primary uk-width-1-1">Search</button>
                                    </div>
                                </div>
                                {this.state.errors.map((item,i) => {
                                    return(
                                        <div className="uk-text-danger">{item}</div>
                                    )
                                })}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProductSearch;