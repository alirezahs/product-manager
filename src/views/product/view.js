import React, {Component} from 'react';
import axios from 'axios';
import { User, Loading, BASE_URL } from '../../inc/functions';

export class Product extends Component{
    constructor(porps){
        super(porps);
        this.state = {
            token: User.getToken(),
            id : this.props.match.params.id,
            comments : [],
            object : {
                instructions: [],
                product:{
                    part:{

                    },
                    installation_item : {

                    },
                    installation_package : {

                    }
                },
                total_instructions : 0,
                done_instructions : 0,
            }
        }
        Loading.start();
    }

    handleDelete = id => {
        if(!window.confirm("Are you sure to delete this item?")) return;
        axios.delete(`${BASE_URL}comments/${id}/${this.state.token}/`)
        .then(res => {
            User.setToken(res.data.token);
            this.load();
        })
        .catch(
            err => {
                Loading.stop();
                this.setState({errors:['Could not delete item.']});
            }
        )
    }

    componentDidMount() {
        this.load();
        this.loadComments();
    }

    load(){
        Loading.start();
        axios
            .get(`${BASE_URL}products/${this.state.id}/${this.state.token}/`)
            .then(
                res => {
                    console.log(res.data);
                    User.setToken(res.data.token);
                    this.setState({
                        object : res.data
                    });
                    Loading.stop();
                }
            )
    }
    loadComments(){
        Loading.start();
        axios
            .get(`${BASE_URL}products/${this.state.id}/comments/${this.state.token}/`)
            .then(
                res => {
                    console.log(res.data);
                    User.setToken(res.data.token);
                    this.setState({
                        comments : res.data.comments
                    });
                    Loading.stop();
                }
            )
    }

    render() {
        return (
            <div>
                <div className="uk-card uk-card-small">
                    <div className="uk-card-header">
                        <h3 className="uk-card-title uk-margin-remove-bottom">Product details</h3>
                    </div>
                    <div className="uk-card-body">
                        <table className="uk-table uk-table-small uk-table-divider">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th className="uk-table-expand">Name</th>
                                    <th className="uk-text-center ">Status</th>
                                    <th className="uk-text-center ">QR Code</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{this.state.object.product.id}</td>
                                    <td>{this.state.object.product.part.name}</td>
                                    <td>
                                        <progress className="uk-progress" value={this.state.object.done_instructions} max={this.state.object.total_instructions}></progress>
                                    </td>
                                    <td className="uk-text-center">
                                        <img width="100" src={"http://dev.constructaware.com/" + this.state.object.product.qr_url}/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table className="uk-table uk-table-small uk-table-divider">
                            <thead>
                                <tr>
                                    <th className="uk-width-1-2">Installation Item</th>
                                    <th className="uk-width-1-2">Installation Package</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><img width="50%" src={ "http://dev.constructaware.com" + this.state.object.product.installation_item.url} /></td>
                                    <td><img width="50%" src={ "http://dev.constructaware.com" + this.state.object.product.installation_package.url} /></td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <h6 className="ml-2 mb-1 uk-text-muted uk-text-uppercase">Instructions</h6>
                        <table className="uk-table uk-table-small uk-table-divider mt-0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Status</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>On</th>
                                    <th>By</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.object.instructions.map((item, i) => {
                                    return(
                                        <tr key={item.id}>
                                            <td>{i+1}</td>
                                            <td><span className={item.product_instruction ? "text-success" : "text-danger"} uk-icon={item.product_instruction ? "check" : "close"}></span></td>
                                            <td>{item.name}</td>
                                            <td>{item.description}</td>
                                            <td>{item.product_instruction ? item.product_instruction.done_on : ""}</td>
                                            <td>{item.product_instruction ? item.product_instruction.done_by.first_name + " " + item.product_instruction.done_by.last_name : ""}</td>
                                        </tr>
                                    )
                                })}
                                
                            </tbody>
                        </table>

                        <h6 className="ml-2 mb-1 uk-text-muted uk-text-uppercase">Comments</h6>
                        <table className="uk-table uk-table-small uk-table-divider mt-0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Author</th>
                                    <th>Comment</th>
                                    {/* <th>Description</th>
                                    <th>On</th>
                                    <th>By</th> */}
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.comments.map((item, i) => {
                                    return(
                                        <tr key={item.id}>
                                            <td>{i+1}</td>
                                            <td>{item.user.first_name + item.user.last_name}</td>
                                            <td>{item.text}</td>
                                            {/* <td>{item.description}</td>
                                            <td>{item.product_instruction ? item.product_instruction.done_on : ""}</td>
                                            <td>{item.product_instruction ? item.product_instruction.done_by.first_name + item.product_instruction.done_by.last_name : ""}</td> */}
                                            <td>
                                                <button onClick={e => this.handleDelete(item.id)} className="uk-button uk-button-danger uk-button-small uk-margin-left">Delete</button>
                                            </td>
                                        </tr>
                                    )
                                })}
                                
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        );
    }
}

export default Product;