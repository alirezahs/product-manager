import React, {Component} from 'react';
import axios from 'axios';
import { User,Loading, BASE_URL } from '../../inc/functions';

export class ProductNew extends Component{
    constructor(props){
        super(props);
        this.state = {
            action : this.props.action === "edit" ? "edit" : "new",
            id: null,
            token: User.getToken(),
            parts: [],
        }
        this.handleFormSubmit = this.handleFormSubmit.bind(this)
        this.fillFormData = this.fillFormData.bind(this)
        if(this.state.action === "edit")
            this.state.id = this.props.match.params.id;
        Loading.start();
    }

    componentDidMount(){
        this.loadParts();
        if(this.state.action == "edit")
            this.fillFormData();
    }

    handleFormSubmit(e){
        e.preventDefault();
        Loading.start();
        let data = {
            "product" : {
                part_id: this.inputPartId.value,
                name: this.inputName.value,
                id: this.inputID.value,
            }
        }
        if(this.state.action=="edit"){
            axios
            .put(`${BASE_URL}products/${this.state.id}/${this.state.token}/`,  data)
            .then(res => {
                console.log(res.data);
                Loading.stop();
                // window.location = `/product/${this.state.id}/edit/`;
            })
        }
        else{
            axios
            .post(`${BASE_URL}products/${this.state.token}/`,  data)
            .then(res => {
                console.log(res.data);
                Loading.stop();
                // window.location = '/products/';
            })
        }
        
    }

    loadParts(){
        Loading.start();
        axios
        .get(`${BASE_URL}parts/${this.state.token}/`)
        .then(
            res => {
                console.log(res.data);
                this.setState({parts: res.data.parts});
                Loading.stop();
            }
        )
    }

    // EDITING MODE
    fillFormData(){
        Loading.start();
        axios
            .get(`${BASE_URL}products/${this.state.id}/${this.state.token}/`)
            .then(
                res => {
                    console.log("EDITING:");
                    console.log(res.data);
                    this.inputPropertyName.value = res.data.part_property.name;
                    this.inputPropertyType.value = res.data.part_property.type;
                    Loading.stop();
                }
            )
    }


    render() {
        return (
            <div className="uk-card uk-card-small">
                <div className="uk-card-header">
                    <div className="row">
                        <div className="col-6">
                            <h3 className="uk-card-title uk-margin-remove-bottom">New Product</h3>
                        </div>
                        <div className="col-6 uk-text-right">
                            <a href="/parts/" className="uk-button uk-button-small uk-button-secondary">Back</a>
                        </div>
                    </div>
                </div>
                <div className="uk-card-body">
                    <form action="#" method="POST" onSubmit={this.handleFormSubmit}>
                        <fieldset className="uk-fieldset">
                            <div className="row">
                                <div className="col-9">
                                    <div className="uk-margin">
                                        <input className="uk-input" name="name" type="text" placeholder="Product Name" ref={el => this.inputName = el}/>
                                    </div>
                                    <div className="uk-margin">
                                        <input className="uk-input" name="id" type="text" placeholder="Product ID" ref={el => this.inputID = el}/>
                                    </div>
                                    <div className="uk-margin">
                                        <label htmlFor="part-parent">Part:</label>
                                        <div className="uk-form-controls">
                                            <select name="parent" className="uk-select" id="part-parent" ref={el => this.inputPartId = el}>
                                                {this.state.parts.map((item, i) => {
                                                    return(
                                                        <option key={item.id} value={item.id}>{item.name}</option>
                                                    )
                                                })}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-3">
                                    <div uk-form-custom="target: true">
                                        <input type="file"/>
                                        <input className="uk-input uk-form-width-medium" type="text" placeholder="Product Picture" disabled/>
                                    </div>
                                    <div className="uk-margin-small">
                                        <div className="uk-cover-container uk-height-small">
                                        <img src="img/upload/part.png" alt="worker" uk-cover="true"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" name="general-submit" className="uk-button uk-button-primary uk-width-1-1">submit</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        );
    }
}

export default ProductNew;