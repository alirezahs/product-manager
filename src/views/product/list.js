import React, {Component} from 'react';
import axios from 'axios';
import { User,Loading, BASE_URL } from '../../inc/functions';
import {Link} from "react-router-dom";

export class ProductsList extends Component{
    constructor(porps){
        super(porps);
        this.state = {
            list: [],
            token: User.getToken(),
            errors : [],
        }
        this.searchProduct = this.searchProduct.bind(this);
        Loading.start();
    }


    componentDidMount() {
        this.load()
    }

    load(){
        Loading.start();
        axios
            .get(`${BASE_URL}products/` + this.state.token + `/`)
            .then(
                res => {
                    console.log(res.data);
                    User.setToken(res.data.token);
                    this.setState({
                        list : res.data.products
                    });
                    Loading.stop();
                }
            )
    }

    handleDelete = id => {
        if(!window.confirm("Are you sure to delete this item?")) return;
        axios.delete(`${BASE_URL}products/${id}/${this.state.token}/`)
        .then(res => {
            User.setToken(res.data.token);
            this.load();
        })
        .catch(
            err => {
                Loading.stop();
                this.setState({errors:['Could not delete item.']});
            }
        )
    }

    searchProduct(e){
        e.preventDefault();
        Loading.start();
        var id = this.inputSearch.value;
        axios
            .get(`${BASE_URL}products/${id}/${this.state.token}`)
            .then(
                res => {
                    console.log(res.data);
                    Loading.stop();
                    window.location = `/product/${res.data.product.id}/`;
                }
            )
            .catch(
                err => {
                    Loading.stop();
                    this.setState({errors:['Product ID not found.']});
                }
            )
    }

    render() {
        return (
            <div>
                <div className="uk-card uk-card-small uk-card-body">
                        <form onSubmit={this.searchProduct}>
                            <div className="uk-margin-small">
                                <div className="row justify-content-center">
                                    <div className="col-4 p-1">
                                        <input className="uk-input" type="text" placeholder="Enter product ID here"  ref={el => this.inputSearch = el} required={true}/>
                                    </div>
                                    <div className="col-2 p-1">
                                        <button type="submit" className="uk-button uk-button-primary uk-width-1-1">Search</button>
                                    </div>
                                </div>
                                {this.state.errors.map((item,i) => {
                                    return(
                                        <div key={i} className="uk-text-danger uk-text-center mt-1">{item}</div>
                                    )
                                })}
                            </div>
                        </form>
                </div>
                <div className="uk-card uk-card-small uk-margin-top">
                    <div className="uk-card-header">
                        <div className="row">
                            <div className="col-6">
                                <h3 className="uk-card-title uk-margin-remove-bottom">List of products</h3>
                            </div>
                            <div className="col-6 uk-text-right">
                                {/* <Link to="/product/new/" className="uk-button uk-button-small uk-button-primary"><span uk-icon="icon:plus; ratio:.7"></span> New</Link> */}
                            </div>
                        </div>
                    </div>
                    <div className="uk-card-body">
                        <table className="uk-table uk-table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Part</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.list.map(
                                    (item, i) => {
                                        return(
                                            <tr key={item.id}>
                                                <td>{i+1}</td>
                                                <td>{item.id}</td>
                                                <td>{item.part.name}</td>
                                                <td>
                                                    <Link to={"/product/" + item.id + "/"} className="uk-button uk-button-default uk-button-small">Show</Link>
                                                    <button onClick={e => this.handleDelete(item.id)} className="uk-button uk-button-danger uk-button-small uk-margin-left">Delete</button>
                                                </td>
                                            </tr>
                                        )
                                    }
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProductsList;