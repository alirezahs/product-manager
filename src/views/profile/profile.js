import React, {Component} from 'react';
import axios from 'axios';
import { User, Loading, BASE_URL } from '../../inc/functions';

export class Profile extends Component{
    constructor(props){
        super(props);
        this.state = {
            token : User.getToken(),
            logo_id : 2,
            logo_url : "",
        }
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.load = this.load.bind(this);
        this.LogoOnChange = this.LogoOnChange.bind(this);
        this.uploadLogoForm = this.uploadLogoForm.bind(this);
        Loading.start();
    }

    componentDidMount(){
        this.load()
    }

    load(){
        axios
        .get(`${BASE_URL}factory/${this.state.token}/`)
        .then(
            res => {
                console.log(res.data);
                this.inputName.value = res.data.factory.title;
                this.inputAddress.value = res.data.factory.address;
                this.inputPhone.value = res.data.factory.phone_number;
                this.inputDesc.value = res.data.factory.description;
                this.setState({
                    logo_id : res.data.factory.logo.id,
                    logo_url : res.data.factory.logo.url,
                });
                Loading.stop();
            }
        );
    }
    
    handleFormSubmit(e){
        Loading.start();
        e.preventDefault();
        let data = {
            "factory" : {
                title: this.inputName.value,
                logo_id: this.state.logo_id,
                description: this.inputDesc.value,
                address: this.inputAddress.value,
                phone_number: this.inputPhone.value,
            }
        }
        axios
        .put(`${BASE_URL}factory/${this.state.token}/`,  data)
        .then(res => {
            console.log(res.data);
            this.load();
        })
        
    }

    
    LogoOnChange(e){
        e.preventDefault();
        this.setState({logo_file :e.target.files[0]});
    }
    uploadLogoForm(e){
        e.preventDefault() // Stop form submit
        let file = e.target.files[0];
        const url = `${BASE_URL}media/${this.state.token}/`;
        const formData = new FormData();
        formData.append('file',file)
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        axios
            .post(url, formData,config)
            .then(res=>{
                console.log(res.data);
                this.setState({logo_id:res.data.new_media.id, logo_url:res.data.new_media.url});
            })
            .catch(e=>{
                alert('خطا در آپلود فایل');
                console.log(e);
            })
    }
    

    render() {
        return (
            <div className="uk-card uk-card-small">
                <div className="uk-card-header">
                    <h3 className="uk-card-title uk-margin-remove-bottom">Factory Profile</h3>
                </div>
                <div className="uk-card-body">
                    <form action="#" method="POST" onSubmit={this.handleFormSubmit}>
                        <fieldset className="uk-fieldset">
                            <legend className="uk-legend">General Information</legend>
                            <div className="uk-margin">
                                <input className="uk-input" name="name" type="text" placeholder="Factory Name" ref={el => this.inputName = el}/>
                            </div>
                            <div className="uk-margin">
                                <input className="uk-input" name="phone" type="text" placeholder="Phone" ref={el => this.inputPhone = el}/>
                            </div>
                            <div className="uk-margin">
                                <textarea className="uk-textarea" name="description" rows="5" placeholder="Description" ref={el => this.inputDesc = el}></textarea>
                            </div>
                            <div className="uk-margin">
                                <textarea className="uk-textarea" name="address" rows="3" placeholder="Address" ref={el => this.inputAddress = el}></textarea>
                            </div>
                            <div className="uk-margin">
                                <div className="row">
                                    <div className="col-4">
                                        <div uk-form-custom="target: true">
                                            <input type="file" onChange={this.uploadLogoForm}/>
                                            <input className="uk-input uk-form-width-medium" type="text" placeholder="Change Logo" disabled/>
                                        </div>
                                        <div className="uk-margin-small">
                                            <div className="uk-cover-container uk-height-small">
                                            <img src={"http://dev.constructaware.com" + this.state.logo_url} alt="Factory" uk-cover="true"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" name="general-submit" className="uk-button uk-button-primary uk-width-1-1">submit</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        );
    }
}

export default Profile;