import React, {Component} from 'react';
import axios from 'axios';
import { User,Loading, BASE_URL } from '../../inc/functions';
import {Link} from "react-router-dom";

export class PartInstructionNew extends Component{
    constructor(props){
        super(props);
        this.state = {
            action : this.props.action === "edit" ? "edit" : "new",
            id: null,
            token: User.getToken(),
        }
        this.handleFormSubmit = this.handleFormSubmit.bind(this)
        this.fillFormData = this.fillFormData.bind(this)
        if(this.state.action === "edit")
            this.state.id = this.props.match.params.id;
        Loading.start();
    }

    componentDidMount(){
        Loading.stop();
        if(this.state.action == "edit")
            this.fillFormData();
    }

    handleFormSubmit(e){
        e.preventDefault();
        Loading.start();
        let data = {
            "instruction":{
                name: this.inputInstructionName.value,
                description: this.inputInstructionDescription.value
            }
        }
        if(this.state.action=="edit"){
            axios
            .put(`${BASE_URL}instructions/${this.state.id}/${this.state.token}/`,  data)
            .then(res => {
                console.log(res.data);
                Loading.stop();
                window.location = `/parts/instructions/`;
            })
        }
        else{
            axios
            .post(`${BASE_URL}instructions/${this.state.token}/`,  data)
            .then(res => {
                console.log(res.data);
                Loading.stop();
                window.location = '/parts/instructions/';
            })
        }
        
    }
    // EDITING MODE
    fillFormData(){
        Loading.start();
        axios
            .get(`${BASE_URL}instructions/${this.state.id}/${this.state.token}/`)
            .then(
                res => {
                    console.log("EDITING:");
                    console.log(res.data);
                    this.inputInstructionName.value = res.data.instruction.name;
                    this.inputInstructionDescription.value = res.data.instruction.description;
                    Loading.stop();
                }
            )
    }

    render() {
        return (
            <div>
                <div className="uk-card uk-card-small">
                    <div className="uk-card-header">
                        <div className="row">
                            <div className="col-6">
                            <h3 className="uk-card-title uk-margin-remove-bottom"> {this.state.action=="new" ? "New" : "Edit" } Part Instruction</h3>
                            </div>
                            <div className="col-6 uk-text-right">
                                <Link to="/parts/instructions/" className="uk-button uk-button-small uk-button-secondary">Back</Link>
                            </div>
                        </div>
                    </div>
                    <div className="uk-card-body">
                        <form action="#" method="POST" onSubmit={this.handleFormSubmit}>
                            <fieldset className="uk-fieldset">
                                <div className="uk-margin">
                                    <input className="uk-input" name="name" type="text" placeholder="Instruction Name" ref={el => this.inputInstructionName = el}/>
                                </div>
                                <div className="uk-margin">
                                    {/* <input className="uk-input" name="name" type="text" placeholder="Instruction Name" ref={el => this.inputInstructionName = el}/> */}
                                    <textarea className="uk-textarea" name="description" placeholder="Description" ref={el => this.inputInstructionDescription = el} >
                                            
                                    </textarea>
                                </div>
                                <button type="submit" name="general-submit" className="uk-button uk-button-primary uk-width-1-1">submit</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default PartInstructionNew;