import React, {Component} from 'react';
import axios from 'axios';
import { User, Loading, BASE_URL } from '../../inc/functions';
import {Link} from "react-router-dom";

export class PartInstructionsList extends Component{
    constructor(porps){
        super(porps);
        this.state = {
            list: [],
            token: User.getToken(),
        }
        this.delete = this.delete.bind(this);
        Loading.start();
    }


    componentDidMount() {
        Loading.stop();
        this.load()
    }

    load(){
        Loading.start();
        axios
            .get(`${BASE_URL}instructions/` + this.state.token + `/`)
            .then(
                res => {
                    console.log(res.data);
                    User.setToken(res.data.token);
                    this.setState({
                        list : res.data.instructions
                    });
                    Loading.stop();
                }
            )
    }

    delete(id){
        if (!window.confirm("Are you sure to delete this item?")) return;
        Loading.start();
        axios
            .delete(`${BASE_URL}instructions/`+id+`/` + this.state.token + `/`)
            .then(
                res => {
                    User.setToken(res.data.token);
                    this.load();
                    Loading.stop();
                }
            );
    }

    render() {
        return (
            <div className="uk-card uk-card-small">
                <div className="uk-card-header">
                    <div className="row">
                        <div className="col-6">
                            <h3 className="uk-card-title uk-margin-remove-bottom">List of instructions</h3>
                        </div>
                        <div className="col-6 uk-text-right">
                            <Link to="/parts/instruction/new/" className="uk-button uk-button-small uk-button-primary"><span uk-icon="icon:plus; ratio:.7"></span> New</Link>
                        </div>
                    </div>
                </div>
                <div className="uk-card-body">
                    <table className="uk-table uk-table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.list.map(
                                (item, i) => {
                                    return(
                                        <tr key={item.id}>
                                            <td>{i+1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.description}</td>
                                            <td>
                                                <button className="uk-button uk-button-danger uk-button-small uk-margin-small-right" onClick={() => this.delete(item.id)}>Delete</button>
                                                <Link to={"/parts/instruction/" + item.id + "/edit/"} className="uk-button uk-button-default uk-button-small">Edit</Link>
                                            </td>
                                        </tr>
                                    )
                                }
                            )}
                        </tbody>
                    </table>
                    {/* <div>
                        <ul className="uk-pagination">
                            <li><a href="#"><span className="uk-margin-small-right" uk-pagination-previous="true"></span> Previous</a></li>
                            <li className="uk-margin-auto-left"><a href="#">Next <span className="uk-margin-small-left" uk-pagination-next="true"></span></a></li>
                        </ul>  
                    </div>     */}
                </div>
            </div>
        );
    }
}

export default PartInstructionsList;