import React, {Component} from 'react';
import {Link} from "react-router-dom";
import axios from 'axios';
import { User, Loading, BASE_URL } from '../../inc/functions';
import  NoImage from "../../static/img/no_image_available.jpg";

export class PartNew extends Component{
    constructor(props){
        super(props);
        this.state = {
            token: User.getToken(),
            fillReady : 0,
            action : this.props.action === "edit" ? "edit" : "new",
            id: null,
            logo_id : 0,
            logo_file : null,
            logo_url : NoImage,
            details : [],
            parents : [],
            properties: [],
            instructions : [],
            // instructions : [{"instruction_id": 1, "priority":1}],
            all_instructions : [],
        }
        this.handleAddProperty = this.handleAddProperty.bind(this);
        this.handleRemoveProperty = this.handleRemoveProperty.bind(this)
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.addInstruction = this.addInstruction.bind(this);
        this.fillFormData = this.fillFormData.bind(this);
        this.set_instruction_value = this.set_instruction_value.bind(this);
        this.set_instruction_priority = this.set_instruction_priority.bind(this);
        if(this.state.action === "edit")
            this.state.id = this.props.match.params.id;
        Loading.start();
    }

    componentDidMount(){
        Loading.stop();
        this.loadParents();
        this.loadProperties();
        this.loadInstructions();
        // if(this.state.action == "edit")
        //     this.fillFormData();
    }

    handleAddProperty(e){
        e.preventDefault();
        let details = this.state.details;
        if(this.state.properties.length == 0) return;
        details.push({
            "name": this.inputNewPropName.selectedOptions[0].text,
            "part_property_id": this.inputNewPropName.value,
            "value": this.inputNewPropVal.value
        });
        var properties = this.state.properties;
        for( var i = 0; i < properties.length; i++){ 
            if ( properties[i].id == this.inputNewPropName.value) {
                properties.splice(i, 1); 
                break;
            }
        }
        this.setState({
            details: details,
            properties : properties
        });
        this.inputNewPropVal.value = "";
    }
    
    handleRemoveProperty(index){
        if(! window.confirm('Remove the property?')) return;
        var details = this.state.details;
        details.splice(index, 1);
        this.setState({
            details : details
        });
    }

    handleFormSubmit(e){
        Loading.start();
        e.preventDefault();
        let data = {
            "part":{
                "name": this.inputPartName.value,
                "description": this.inputPartDesc.value,
                "logo_id": this.state.logo_id,
                "parent_id": this.inputPartParent.value,
                "part_details": this.state.details,
                "part_instructions": this.state.instructions,
                // "installation_package_id": this.state.installP_id,
                // "installation_item_id": this.state.installI_id,
            }
        }
        if(this.state.action=="edit"){
            axios
            .put(`${BASE_URL}parts/${this.state.id}/${this.state.token}/`,  data)
            .then(res => {
                // window.location = `/part/${this.state.id}/edit/`;
                Loading.stop();
                window.location = `/parts/`;
            })
        }
        else{
            axios
            .post(`${BASE_URL}parts/${this.state.token}/`,  data)
            .then(res => {
                Loading.stop();
                window.location = '/parts/';
            })
        }
        
    }

    loadParents(){
        Loading.start();
        axios
            .get(`${BASE_URL}parts/${this.state.token}/`)
            .then(res => {
                this.setState({parents : res.data.parts});
                Loading.stop();
            });
    }

    loadProperties(){
        Loading.start();
        axios
            .get(`${BASE_URL}part_properties/${this.state.token}/`)
            .then(res => {
                console.log(res.data);
                this.setState({properties : res.data.part_properties, fillReady : this.state.fillReady + 1});
                if(this.state.fillReady > 1 && this.state.action == "edit")
                    this.fillFormData();
                Loading.stop();
            });
    }

    loadInstructions(){
        Loading.start();
        axios
            .get(`${BASE_URL}instructions/` + this.state.token + `/`)
            .then(
                res => {
                    User.setToken(res.data.token);
                    this.setState({
                        all_instructions : res.data.instructions,
                        fillReady: this.state.fillReady + 1
                    });
                    if(this.state.fillReady > 1 && this.state.action == "edit")
                        this.fillFormData();
                    Loading.stop();
                }
            )
    }

    LogoOnChange(e, type){
        Loading.start();
        e.preventDefault();
        let file = e.target.files[0];
        const url = `${BASE_URL}media/${this.state.token}/`;
        const formData = new FormData();
        formData.append('file',file)
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        axios
            .post(url, formData,config)
            .then(res=>{
                console.log(res.data);
                switch(type){
                    case "logo":
                        this.setState({logo_id:res.data.new_media.id, logo_url:"http://dev.constructaware.com" + res.data.new_media.url});
                        break;
                    case "package":
                        this.setState({installP_id:res.data.new_media.id, installP_url:"http://dev.constructaware.com" + res.data.new_media.url});
                        break;
                    case "item":
                        this.setState({installI_id:res.data.new_media.id, installI_url:"http://dev.constructaware.com" + res.data.new_media.url});
                        break;
                    default:break;
                }
                Loading.stop();
            })
            .catch(e=>{
                alert('Could not upload the image.');
                console.log(e);
            })
        
    }

    // EDITING MODE
    fillFormData(){
        console.log(this.state.fillReady);
        Loading.start();
        axios
            .get(`${BASE_URL}parts/${this.state.id}/${this.state.token}/`)
            .then(
                res => {
                    console.log(res.data);
                    this.inputPartName.value = res.data.part.name;
                    this.inputPartParent.value = res.data.part.parent.id ? res.data.part.parent.id : 0;
                    this.inputPartDesc.value = res.data.part.description;
                    let details = res.data.part.part_details.map((item, i) => {
                        return {
                            part_property_id : item.id,
                            value: item.value,
                            name: item.part_property.name,
                        }
                    });
                    let instructions = res.data.part.part_instructions.map((item,i)=>{
                        return {
                            instruction_id : item.instruction.id,
                            priority : item.priority
                        };
                    })
                    let logo_id = res.data.part.logo.id ? res.data.part.logo.id : 0;
                    let installP_id = res.data.part.installation_package ? res.data.part.installation_package.id : 0;
                    let installI_id = res.data.part.installation_item ? res.data.part.installation_item.id : 0;
                    this.setState({
                            details: details,
                            instructions:instructions,
                            logo_id : logo_id,
                            installP_id: installP_id,
                            installI_id: installI_id,
                            logo_url : "http://dev.constructaware.com" + res.data.part.logo.url,
                            installI_url : res.data.part.installation_item ? "http://dev.constructaware.com" + res.data.part.installation_item.url : NoImage,
                            installP_url : res.data.part.installation_package ? "http://dev.constructaware.com" + res.data.part.installation_package.url : NoImage,
                        }
                    );
                    console.log(this.state.instructions);
                }
            )
    }

    addInstruction(e){
        e.preventDefault();
        if(this.state.instructions.length >= this.state.all_instructions.length){
            alert("No more instruction !");
            return false;
        }
        let curr = this.state.instructions;
        curr.push({"instruction_id": this.state.all_instructions[curr.length].id, "priority": curr.length+1});
        this.setState({instructions: curr});
        // console.log(curr);
    }
    removeInstruction(e, i){
        e.preventDefault();
        let curr = this.state.instructions;
        curr.splice(i, 1);
        this.setState({instructions: curr});
    }

    set_instruction_value(e, index){
        let curr_inst = this.state.instructions;
        // for(var i=0;i<curr_inst.length;i++){
        //     if(curr_inst[i].instruction_id == e.target.value){
        //         e.preventDefault();
        //         alert('Duplicate instruction !');
        //         return;
        //     }
        // }
        curr_inst[index].instruction_id = parseInt(e.target.value);
        this.setState({instructions: curr_inst});
    }
    set_instruction_priority(e, index){
        let curr_inst = this.state.instructions;
        curr_inst[index].priority = e.target.value;
        this.setState({instructions: curr_inst});
    }

    render() {
        return (
            <div>
                <div className="uk-card uk-card-small">
                    <div className="uk-card-header">
                        <div className="row">
                            <div className="col-6">
                                <h3 className="uk-card-title uk-margin-remove-bottom"> {this.state.action=="new" ? "New" : "Edit" } Part</h3>
                            </div>
                            <div className="col-6 uk-text-right">
                                <Link to="/parts/" className="uk-button uk-button-small uk-button-secondary">Back</Link>
                            </div>
                        </div>
                    </div>
                    <div className="uk-card-body">
                        <form action="#" method="POST" onSubmit={this.handleFormSubmit}>
                            <fieldset className="uk-fieldset">
                                    <div className="uk-margin">
                                        <label>Name:</label>
                                        <input className="uk-input" name="name" type="text" placeholder="Part Name" ref={el => this.inputPartName = el}/>
                                    </div>
                                    <div className="uk-margin">
                                        <label>Parent:</label>
                                        <div className="uk-form-controls">
                                            <select name="parent" className="uk-select" id="part-parent" ref={el => this.inputPartParent = el}>
                                                <option value="0">[No Parent]</option>
                                                {this.state.parents.map((item, i) => {
                                                    return(
                                                        <option key={item.id} value={item.id}>{item.name}</option>        
                                                    )
                                                })}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="uk-margin">
                                        <label>Description:</label>
                                        <textarea className="uk-textarea" name="description" rows="5" placeholder="Description" ref={el => this.inputPartDesc = el}></textarea>
                                    </div>
                                    <div className="row">
                                        <div className="col-4">
                                            <div className="uk-margin">
                                                <label>Featured Image</label>
                                                <div uk-form-custom="target: true" className="uk-width-1-1">
                                                    <img width="100%" className="uk-height-small" src={this.state.logo_url ? this.state.logo_url : NoImage}/>
                                                    <input type="file" onChange={(e) => this.LogoOnChange(e, "logo")}/>
                                                    <input className="uk-input uk-input-small uk-margin-small-top" type="text" placeholder="Select Image" disabled />
                                                </div>
                                                {/* <button  onClick={(e) => this.uploadLogoForm(e, "plan")} className="uk-button uk-margin-small-top uk-width-1-1 uk-button-default uk-button-small">Upload Plan</button> */}
                                            </div>
                                        </div>
                                        {/* <div className="col-4">
                                            <div className="uk-margin">
                                                <label>Installation Package</label>
                                                <div uk-form-custom="target: true" className="uk-width-1-1">
                                                    <img width="100%" className="uk-height-small" src={this.state.installP_url}/>
                                                    <input type="file" onChange={(e) => this.LogoOnChange(e, "package")}/>
                                                    <input className="uk-input uk-input-small uk-margin-small-top" type="text" placeholder="Select Image" disabled />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-4">
                                            <div className="uk-margin">
                                                <label>Installation Item</label>
                                                <div uk-form-custom="target: true" className="uk-width-1-1">
                                                    <img width="100%" className="uk-height-small" src={this.state.installI_url}/>
                                                    <input type="file" onChange={(e) => this.LogoOnChange(e, "item")}/>
                                                    <input className="uk-input uk-input-small uk-margin-small-top" type="text" placeholder="Select Image" disabled />
                                                </div>
                                            </div>
                                        </div> */}
                                    </div>
                                    <div className="uk-margin">
                                        <div className="row">
                                            <div className="col-6">
                                                <h6>Properties:</h6>
                                            </div>
                                            <div className="col-6 uk-text-right">
                                                <button type="button" className="uk-button uk-button-danger" uk-toggle="target: #modal-add-property">Add</button>
                                            </div>
                                        </div>
                                        
                                        <table id="properties" className="uk-table uk-table-divider uk-table-small uk-table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Value</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.details.map(
                                                    (item, i) => {
                                                        return(
                                                            <tr key={i}>
                                                                <td>{i+1}</td>
                                                                <td className="uk-text-capitalize">{item.name}</td>
                                                                <td className="uk-text-capitalize">{item.value}</td>
                                                                <td><button type="button" onClick={(i) => this.handleRemoveProperty(i)} className="uk-button uk-button-default uk-button-small">Remove</button></td>
                                                            </tr>
                                                        )
                                                    }
                                                )}
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="uk-margin">
                                        <div className="row">
                                            <div className="col-6">
                                                <h6>Instructions:</h6>
                                            </div>
                                            <div className="col-6 uk-text-right">
                                                <button type="button" className="uk-button uk-button-danger" onClick={this.addInstruction}>Add</button>
                                            </div>
                                        </div>
                                        
                                        
                                        <table id="instructions" className="uk-table uk-table-divider uk-table-small uk-table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Instruction</th>
                                                    <th>Priority</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.instructions.map((item, i) => {
                                                    return(
                                                        <tr key={i}>
                                                            <td>{i+1}</td>
                                                            <td>
                                                                <select className="uk-select" defaultValue={item.instruction_id} onChange={(e)=> this.set_instruction_value(e, i)}>
                                                                    {this.state.all_instructions.map((item_i, i_i) => {
                                                                        return(
                                                                            <option value={item_i.id} key={item_i.id}>{item_i.name}</option>
                                                                        )
                                                                    })}
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select className="uk-select" defaultValue={item.priority} onChange={(e) => this.set_instruction_priority(e, i)}>
                                                                    {this.state.instructions.map((item,i)=>{
                                                                        return(
                                                                            <option key={i} value={i+1}>{i+1}</option>
                                                                        );
                                                                    })}
                                                                </select>
                                                            </td>
                                                            <td className="uk-text-center">
                                                                <button className="uk-button uk-button-default" onClick={(e) => this.removeInstruction(e, i)}>Remove</button>
                                                                
                                                            </td>
                                                        </tr>
                                                        
                                                    )
                                                })}
                                            </tbody>
                                        </table>
                                    </div>
                                    <button type="submit" name="general-submit" className="uk-button uk-button-primary uk-width-1-1" >submit</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <div id="modal-add-property" uk-modal="true">
                    <div className="uk-modal-dialog ">
                        <div className="uk-modal-header">
                            <h2 className="uk-modal-title">Add Property</h2>
                        </div>
                        <div className="uk-modal-body">
                            <p>Select property and enter value:</p>
                            <div className="uk-form-horizontal">
                                <div className="uk-margin">
                                    <label className="uk-form-label">Property name:</label>
                                    <div className="uk-form-controls">
                                        {/* <input className="uk-input" type="text" placeholder="Enter proprty name" ref={el => this.inputNewPropName = el}/> */}
                                        <select id="property-name" className="uk-select" name="property-name" ref={el => this.inputNewPropName = el}>
                                            {this.state.properties.map((item, i) => {
                                                return(
                                                    <option key={item.id} value={item.id}>{item.name}</option>        
                                                )
                                            })}
                                        </select>
                                    </div>
                                </div>
                                <div className="uk-margin">
                                    <label className="uk-form-label">Property Value:</label>
                                    <div className="uk-form-controls">
                                        <input className="uk-input" type="text" name="property-value" id="property-value" placeholder="Enter proprty value" ref={el => this.inputNewPropVal = el}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="uk-modal-footer uk-text-right">
                            <button className="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                            <a href="#" className="uk-button uk-button-primary uk-modal-close" onClick={this.handleAddProperty}>Insert</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PartNew;