import React, {Component} from 'react';
import {Link} from "react-router-dom";
import axios from 'axios';
import { User, Loading, BASE_URL } from '../../inc/functions';
import  NoImage from "../../static/img/no_image_available.jpg";

export class PartsList extends Component{
    constructor(porps){
        super(porps);
        this.state = {
            list: [],
            token: User.getToken(),
            installP_id : 0,
            installP_url : NoImage,
            installI_id : 0,
            installI_url : NoImage,
            part_id : null,
        }
        this.delete = this.delete.bind(this);
        this.newProduct = this.newProduct.bind(this);
        this.LogoOnChange = this.LogoOnChange.bind(this)
        this.prepareNewProductModal = this.prepareNewProductModal.bind(this);
    }


    componentDidMount() {
        this.load()
    }

    load(){
        Loading.start();
        axios
            .get(`${BASE_URL}parts/` + this.state.token + `/`)
            .then(
                res => {
                    console.log(res.data);
                    User.setToken(res.data.token);
                    this.setState({
                        list : res.data.parts
                    });
                    Loading.stop();
                }
            )
    }

    delete(id){
        if (!window.confirm("Are you sure to delete this item?")) return;
        Loading.start();
        axios
            .delete(`${BASE_URL}parts/`+id+`/` + this.state.token + `/`)
            .then(
                res => {
                    User.setToken(res.data.token);
                    this.load();
                    Loading.stop();
                }
            );
    }

    newProduct(){
        Loading.start();
        let data = {
            "product" : {
                "part_id": this.state.part_id,
                "installation_package_id": this.state.installP_id,
                "installation_item_id": this.state.installI_id,
            }
        }
        axios
            .post(`${BASE_URL}products/${this.state.token}/`,  data)
            .then(res => {
                Loading.stop();
                window.location = `/product/${res.data.product.id}/`;
            })
    }

    prepareNewProductModal(part_id){
        this.setState({part_id:part_id})
    }

    LogoOnChange(e, type){
        Loading.start();
        e.preventDefault();
        let file = e.target.files[0];
        const url = `${BASE_URL}media/${this.state.token}/`;
        const formData = new FormData();
        formData.append('file',file)
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        axios
            .post(url, formData,config)
            .then(res=>{
                console.log(res.data);
                switch(type){
                    case "logo":
                        this.setState({logo_id:res.data.new_media.id, logo_url:"http://dev.constructaware.com" + res.data.new_media.url});
                        break;
                    case "package":
                        this.setState({installP_id:res.data.new_media.id, installP_url:"http://dev.constructaware.com" + res.data.new_media.url});
                        break;
                    case "item":
                        this.setState({installI_id:res.data.new_media.id, installI_url:"http://dev.constructaware.com" + res.data.new_media.url});
                        break;
                    default:break;
                }
                Loading.stop();
            })
            .catch(e=>{
                alert('Could not upload the image.');
                console.log(e);
            })
        
    }

    render() {
        return (
            <div >
                <div className="uk-card uk-card-small">
                    <div className="uk-card-header">
                        <div className="row">
                            <div className="col-6">
                                <h3 className="uk-card-title uk-margin-remove-bottom">List of parts</h3>
                            </div>
                            <div className="col-6 uk-text-right">
                                <Link to="/part/new/" className="uk-button uk-button-small uk-button-primary"><span uk-icon="icon:plus; ratio:.7"></span> New</Link>
                            </div>
                        </div>
                    </div>
                    <div className="uk-card-body">
                        <table className="uk-table uk-table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Parent</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.list.map(
                                    (item, i) => {
                                        return(
                                            <tr key={item.id}>
                                                <td>{i+1}</td>
                                                <td><img src={"http://dev.constructaware.com" + item.logo.url} width="40"/></td>
                                                <td>{item.name}</td>
                                                <td>{item.parent.name}</td>
                                                <td>
                                                    <button className="uk-button uk-button-danger uk-button-small uk-margin-small-right" onClick={() => this.delete(item.id)}>Delete</button>
                                                    <Link to={"/part/" + item.id + "/edit/"} className="uk-button uk-button-default uk-button-small">Edit</Link>
                                                    <button className="uk-button uk-button-primary uk-button-small uk-margin-small-left"  onClick={() => this.prepareNewProductModal(item.id)} uk-toggle="target: #modal-product">New Product</button>
                                                    {/* onClick={() => this.newProduct(item.id)} */}
                                                </td>
                                            </tr>
                                        )
                                    }
                                )}
                            </tbody>
                        </table>
                        {/* <div>
                            <ul className="uk-pagination">
                                <li><a href="#"><span className="uk-margin-small-right" uk-pagination-previous="true"></span> Previous</a></li>
                                <li className="uk-margin-auto-left"><a href="#">Next <span className="uk-margin-small-left" uk-pagination-next="true"></span></a></li>
                            </ul>  
                        </div>     */}
                    </div>
                </div>
                <div id="modal-product" uk-modal="true">
                    <div className="uk-modal-dialog ">
                        <div className="uk-modal-header">
                            <h2 className="uk-modal-title">New Product</h2>
                        </div>
                        <div className="uk-modal-body">
                            <div class="row">
                                <div className="col-6">
                                    <div className="uk-margin">
                                        <label>Installation Package</label>
                                        <div uk-form-custom="target: true" className="uk-width-1-1">
                                            <img width="100%" className="uk-height-small" src={this.state.installP_url}/>
                                            <input type="file" onChange={(e) => this.LogoOnChange(e, "package")}/>
                                            <input className="uk-input uk-input-small uk-margin-small-top" type="text" placeholder="Select Image" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-6">
                                    <div className="uk-margin">
                                        <label>Installation Item</label>
                                        <div uk-form-custom="target: true" className="uk-width-1-1">
                                            <img width="100%" className="uk-height-small" src={this.state.installI_url}/>
                                            <input type="file" onChange={(e) => this.LogoOnChange(e, "item")}/>
                                            <input className="uk-input uk-input-small uk-margin-small-top" type="text" placeholder="Select Image" disabled />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="uk-modal-footer uk-text-right">
                            <button className="uk-button uk-button-default uk-margin-small-right uk-modal-close" type="button">Cancel</button>
                            <a href="#" className="uk-button uk-button-primary uk-modal-close" onClick={this.newProduct}>Submit</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PartsList;