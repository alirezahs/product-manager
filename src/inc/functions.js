import axios from 'axios';
import $ from "jquery";

export const BASE_URL = 'http://dev.constructaware.com/api/';
// export const BASE_MEDIA = 'http://dev.constructaware/';

export var User = (function() {

    var login = (creds, redirect=false) => {
        var data = {
            'user': creds,
        }
        axios
        .post(`${BASE_URL}auth/sign_in_admin/`, data)
            .then(res => {
                Loading.stop();
                var user = res.data.user;
                // var token = "test_token";
                var token = res.data.token;
                setUser(user);
                setToken(token);
                if(redirect)
                    window.location = '/';
            })
            .catch((err) => {
                Loading.stop();
                if(err.response){
                    switch(err.response.data.message){
                        case 'USER_ACCOUNT_NOT_ACTIVE':
                            var message = "حساب غیرفعال است."
                            break;
                        default:
                            var message = "نام کاربری یا رمزعبور نامعتبر است."
                            break;
                    }
                }
                else
                    var message = "ورود ناموفق بود!";
                alert(message);
            });
    }

    var logout = () => {
        // var token = getToken();
        // axios
        //     .get(`http://dev.kishfish.coleocc.ir/api/auth/sign_out/` + token + `/`)
        //     .then(res => {
                setToken(null);
                setUser(null);
                window.location = '/login/';
            // });
    }

    var is_user = () => {
        if(getToken())
            return true;
        setToken(null);
        return false
    }

    var is_admin = () => {
        if(is_user()){
            var user = getUser();
            if(!user) return false;
            return user.is_admin;
        }
        return false;
    }

    var setToken = (new_token) => {
        if(new_token===null)
            window.sessionStorage.removeItem("token");
        else
            window.sessionStorage.setItem("token", new_token)
    }

    var getToken = () => {
        var token = window.sessionStorage.getItem("token");
        return token;
    }
    
    var setUser = (user) => {
        if(user===null)
            window.sessionStorage.removeItem("user");
        else{
            window.sessionStorage.setItem("user", JSON.stringify(user))
        }
    }

    var getUser = () => {
        var user = window.sessionStorage.getItem("user");
        console.log(user);
        return JSON.parse(user);
    }

    return {
        setToken : setToken,
        getToken : getToken,
        login : login,
        logout : logout,
        is_user : is_user,
        is_admin : is_admin,
        getUser : getUser,
        setUser : setUser,
    }
})();


// export class Loading{
//     start(){
//         alert("started");
//         $("#loading").show();
//     }
//     stop(){
//         $("#loading").hide();
//     }
// }

export var Loading = (function() {
    var start = () => {
        $("#loading").css("display", "flex");
        $(".main").css("overflow", "none");
    }
    var stop = () => {
        $("#loading").hide();
        $(".main").css("overflow", "auto");
    }
    return{
        start: start,
        stop : stop
    }
})();