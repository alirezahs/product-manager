import React, { Component } from 'react';
import {User} from './functions';

export class Header extends Component {
    render(){
        return(
            <header>
                <div className="my-5">
                    <h1 className="uk-text-center">Product Manager</h1>
                </div>
            </header>
        )
    }
}

export class Footer extends Component {
    render(){
        return(
            <footer>
                
            </footer>
        )
    }
}

export class Auth extends Component{
    constructor(props){
        super(props);
        this.state = {
            is_user : User.is_user(),
            is_admin : User.is_admin(),
            show : false,
            redirect : false,
        }
        if(props.redirect)
            this.state.redirect = true;
            
        switch(props.access){
            case 'user':
                this.state.show = this.state.is_user;
                break;
            case 'only-user':
                this.state.show = this.state.is_user && !this.state.is_admin;
                break;
            case 'admin':
                this.state.show = this.state.is_admin;
                break;
            case 'not-user':
                if(this.state.redirect)
                    this.redirectLoginPage();
                this.state.show = (!this.state.is_user);
                break;
            default:
                break;
        }
    }

    redirectLoginPage(){
        if(window.location.pathname != '/login/')
            window.location = '/login/';
    }
    redirectHomePage(){
        if(window.location.pathname != '/')
            window.location = '/';
    }

    render(){
        return(
            this.state.show && this.props.children ? this.props.children : null
        )
    }
}