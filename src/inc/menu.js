import React, {Component} from 'react'
import {User, Loading} from './functions';
import {Link} from "react-router-dom";

export class Menu extends Component{
    constructor(props){
        super(props);
        this.logout = this.logout.bind(this);
    }

    logout(e){
        e.preventDefault();
        Loading.start();
        if(!window.confirm("You're logging out from panel")) return;
        User.logout();
    }
    render(){
        return(
            <div className="menu">
                <ul className="uk-nav uk-padding-small uk-nav-parent-icon">
                    <li><Link to="/"><span className="uk-margin-small-right" uk-icon="cog"></span>Factory Profile</Link></li>
                    <li><Link to="/employees/"><span className="uk-margin-small-right" uk-icon="icon: users"></span>Employees</Link></li>
                    <li className="uk-parent">
                        <Link to="/parts/"><span className="uk-margin-small-right" uk-icon="nut"></span>Parts</Link>
                        <ul className="uk-nav-sub">
                            <li><Link to="/parts/properties/">Properties</Link></li>
                            <li><Link to="/parts/instructions/">Instructions</Link></li>
                        </ul>
                    </li>
                    <li><Link to="/products/"><span className="uk-margin-small-right" uk-icon="thumbnails"></span>Products</Link></li>
                    <li className="uk-nav-divider"></li>
                    <li><Link to="/" onClick={this.logout}><span className="uk-margin-small-right" uk-icon="sign-out"></span>Sign Out</Link></li>
                </ul>
            </div>
        )
    }
}